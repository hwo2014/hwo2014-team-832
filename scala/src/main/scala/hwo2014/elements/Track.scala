package main.scala.hwo2014.elements

case class TrackData (id: String, name: String)
case class Track(track: TrackData)
