package test.scala.hwo2014

import org.scalatest._
import org.scalatest.Assertions._
import org.json4s.native.Serialization.{read}
import org.json4s.DefaultFormats
import org.json4s._
import org.json4s.native.JsonMethods._

import main.scala.hwo2014.elements._

class FirstSpec extends FlatSpec {
  implicit val formats = new DefaultFormats{}

  "A test assert" should "return true" in {
    val a = 5
    val b = 2
    assertResult(3) {
      a - b
    }
  }

  "Test json " should "be parsed" in {
    val json = """ {"hello": "world"} """
    println(parse(json))
  }

  "A test " should "parse json" in {
    //val gameInit = Serialization.read[GameInit](json)

    val track = parse(trackJson)
    println(track.extract[Track])
    //println(track)

    val race = parse(raceJson)
    println(race.extract[Race])
    //println(race)

    //println(gameInit)
    //println(gameInit.data)
    //println(gameInit.data.track)

  }
  val trackJson = """{ "track": {"id": "indianapolis", "name": "Indianapolis"}} """

  val raceJson = """ {"race": {"track": {"id": "indianapolis", "name": "Indianapolis"}}} """

  val json = """
            { "msgType": "gameInit",
              "data": {
                "race": {
                  "track": { "id": "indianapolis", "name": "Indianapolis" }
                }
              }
            }
             """
}
